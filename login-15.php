<?php

if(isset($_POST['search']))
{
    $valueToSearch = $_POST['valueToSearch'];
    // search in all table columns
    // using concat mysql function
    $query = "SELECT * FROM `visitor` WHERE CONCAT(`id`, `name`, `shopName`, `mobileNo`,`enterTime`, `exitTime` ) LIKE '%".$valueToSearch."%'";
    $search_result = filterTable($query);
    
}
 else {
    $query = "SELECT * FROM `visitor`";
    $search_result = filterTable($query);
}

// function to connect and execute the query
function filterTable($query)
{
    $connect = mysqli_connect("localhost", "root", "", "ctracker");
    $filter_Result = mysqli_query($connect, $query);
    return $filter_Result;
}

?>





<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="Contact Verification" />
<meta name="author" content="potenzaglobalsolutions.com" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>Contact Verification</title>

<!-- Favicon -->
<link rel="shortcut icon" href="images/favicon.ico" />

<!-- font -->
<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,500,500i,600,700,800,900|Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
 
<!-- Plugins -->
<link rel="stylesheet" type="text/css" href="css/plugins-css.css" />

<!-- Typography -->
<link rel="stylesheet" type="text/css" href="css/typography.css" />

<!-- Shortcodes -->
<link rel="stylesheet" type="text/css" href="css/shortcodes/shortcodes.css" />

<!-- Style -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Responsive -->
<link rel="stylesheet" type="text/css" href="css/responsive.css" /> 

<style>
            table,tr,th,td
            {
              margin: start;
              width: 10%;
              border: 2px solid #73AD21;
              padding: 3px;
              
            }
            
        </style>
   
</head>

<body>
 
<div class="wrapper">

<!--=================================
 preloader -->
 
<div id="pre-loader">
    <img src="images/pre-loader/loader-09.svg" alt="">
</div>

<!--=================================
 preloader -->

<!--=================================
login -->

<section class="login white-bg o-hidden scrollbar">
  <div class="container-fluid p-0">
    <div class="row no-gutter">
      <div class="col-xl-3 col-lg-5">
        <div class="d-flex align-items-center height-100vh full-width">
        <div class="login-15">
          <h1> COVID TRACKER</h1>
          <p class="mb-30"> </p>
          <div class="pb-50 clearfix white-bg">
          <form action="login-15.php" method="post">
            <div class="section-field mb-20">
             <label class="mb-10" for="phone">Phone Number* </label>
             <input type="text" name="valueToSearch" placeholder="Value To Search">
            </div>
            <div class="section-field">
              
              </div>
              <input type="submit" name="search" value="Filter">
             <p class="mt-20 mb-0"> <a href="signup-15.html"> </a></p>
          </form>
          </div>
          
        </div>
      </div>
     </div>
     <div class="col-xl-9 col-lg-7 parallax" style="background-image: url(images/login/13.jpg);">
     </div>
     <div style="padding:3%">
     <div class="table">
        <h2>Result</h2>
        <p class="mb-30"> </p>
        
         <div class="section-field mb-20>
           <label class="mb-30" for="name"><b>LOCATIONS</b></label>
             <div style="padding-top:5%">
             <table>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Shop Name</th>
                    <th>Phone</th>
                    <th>Enter Time</th>
                    <th>Exit Time</th>
                </tr>

      <!-- populate table from mysql database -->
                <?php while($row = mysqli_fetch_array($search_result)):?>
                <tr>
                    <td><?php echo $row['id'];?></td>
                    <td><?php echo $row['name'];?></td>
                    <td><?php echo $row['shopName'];?></td>
                    <td><?php echo $row['mobileNo'];?></td>
                    <td><?php echo $row['enterTime'];?></td>
                    <td><?php echo $row['exitTime'];?></td>
                </tr>
                <?php endwhile;?>
             </table>
                </div>
            </div>
                </div>
       </div>
    </div>
  </div>
</section>

<!--=================================
 login -->
  
</div>
 
<div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-angle-up"></i> <span>TOP</span></a></div>
 
<!--=================================
 jquery -->

<!-- jquery -->
<script src="js/jquery-3.3.1.min.js"></script>

<!-- plugins-jquery -->
<script src="js/plugins-jquery.js"></script>

<!-- plugin_path -->
<script>var plugin_path = 'js/';</script>
 
<!-- custom -->
<script src="js/custom.js"></script>

 
 
</body>
</html>